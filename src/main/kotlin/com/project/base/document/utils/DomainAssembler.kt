package com.project.base.document.utils

import org.apache.commons.io.IOUtils
import java.io.InputStream

fun strToInputStream(str: String): InputStream = IOUtils.toInputStream(str, Charsets.UTF_8)

fun inputStreamToString(inputStream: InputStream): String = IOUtils.toString(inputStream, Charsets.UTF_8)
