package com.project.base.document.root

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.server.ServerWebExchange
import java.net.URI

@RestController
@RequestMapping
class RootRestControlller {

    @GetMapping
    fun getBasicInformation(webExchange: ServerWebExchange): ResponseEntity<Void> {
        val request = webExchange.request

        return ResponseEntity
            .status(HttpStatus.FOUND)
            .location(
                URI.create("${request.uri}actuator/info")
            ).build()
    }

}