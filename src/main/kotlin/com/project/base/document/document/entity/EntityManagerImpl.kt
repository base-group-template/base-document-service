package com.project.base.document.document.entity

import com.project.base.document.document.domain.toDataBufferFlux
import com.project.base.document.document.domain.toFileData
import com.project.base.document.document.domain.toMetadata
import com.project.base.document.document.repository.DocumentRepository
import com.project.base.document.document.repository.ProcessedDocumentRepository
import org.bson.types.ObjectId
import org.springframework.data.mongodb.core.query.Criteria
import org.springframework.data.mongodb.core.query.Query
import org.springframework.data.mongodb.gridfs.GridFsCriteria
import org.springframework.data.mongodb.gridfs.ReactiveGridFsTemplate
import org.springframework.stereotype.Component
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toFlux


@Component
class EntityManagerImpl(
    private val documentRepository: DocumentRepository,
    private val processedDocumentRepository: ProcessedDocumentRepository,
    private val gridFsTemplate: ReactiveGridFsTemplate
) : EntityManager {
    override fun saveDocument(document: Document): Mono<Document> = documentRepository.save(document)

    override fun findDocument(id: String): Mono<Document> = documentRepository.findById(ObjectId(id))

    override fun findAllDocumentsByUserId(userId: String): Flux<Document> = documentRepository.findAllByUserId(userId)

    override fun deleteDocument(id: String): Mono<Boolean> =
        documentRepository.deleteById(ObjectId(id)).thenReturn(true)

    override fun deleteAllDocuemnts(userId: String): Mono<Boolean> =
        documentRepository.findAllByUserId(userId)
            .collectList()
            .flatMap { documents ->
                val filesIds = documents.map { it.fileId }

                deleteAllFiles(filesIds)
                    .flatMap { documentRepository.deleteAll(documents).thenReturn(true) }
            }

    override fun saveFile(fileData: FileData): Mono<ObjectId> =
        gridFsTemplate.store(
            fileData.inputStream.toDataBufferFlux(),
            fileData.name,
            fileData.mediaType,
            fileData.toMetadata()
        )

    override fun saveAllFiles(files: List<FileData>): Flux<ObjectId> = files.toFlux().flatMap { saveFile(it) }

    override fun findFile(id: ObjectId): Mono<FileData> = gridFsTemplate.findOne(queryById(id)).map { it.toFileData() }

    override fun deleteFile(id: ObjectId): Mono<Boolean> = gridFsTemplate.delete(queryById(id)).thenReturn(true)

    override fun deleteAllFiles(ids: List<ObjectId>): Mono<Boolean> =
        gridFsTemplate.delete(queryById(ids)).thenReturn(true)

    override fun deleteProcessedDocument(id: ObjectId): Mono<Boolean> =
        processedDocumentRepository.deleteById(id).thenReturn(true)

    private fun queryById(fileId: ObjectId): Query = Query(GridFsCriteria.where("_id").`is`(fileId))

    private fun queryById(filesIds: List<ObjectId>): Query = Query(Criteria.where("_id").`in`(filesIds))
}
