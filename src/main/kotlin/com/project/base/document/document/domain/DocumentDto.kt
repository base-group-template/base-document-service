package com.project.base.document.document.domain

data class DocumentDto(
    val id: String?,
    val userId: String,
    val type: DocumentClass,
    val fileId: String,
    var processedDocumentId: String
) {
    enum class DocumentClass {
        RECEIPT
    }
}