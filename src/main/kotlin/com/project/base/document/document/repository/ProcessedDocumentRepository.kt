package com.project.base.document.document.repository

import com.project.base.document.document.entity.ProcessedDocument
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.ReactiveMongoRepository

interface ProcessedDocumentRepository : ReactiveMongoRepository<ProcessedDocument, ObjectId>