package com.project.base.document.document.domain.requests

import com.project.base.document.document.domain.CreateDocumentDto

data class ProcessFiles(
    val documents: List<CreateDocumentDto>
)