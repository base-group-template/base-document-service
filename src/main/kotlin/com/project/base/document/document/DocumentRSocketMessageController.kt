package com.project.base.document.document

import com.project.base.document.document.domain.DocumentDto
import com.project.base.document.document.domain.requests.*
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface DocumentRSocketMessageController {

    fun saveDocument(request: SaveDocument): Mono<DocumentDto>

    fun findDocument(request: FindDocument): Mono<DocumentDto>

    fun findAllDocuments(metadata: Map<String, Any>, request: FindAllDocuments): Flux<DocumentDto>

    fun updateDocument(request: UpdateDocument): Mono<DocumentDto>

    fun deleteDocument(request: DeleteDocument): Mono<Boolean>

    fun deleteAllDocuments(request: DeleteAllDocuments): Mono<Boolean>

    fun processFiles(metadata: Map<String, Any>, request: ProcessFiles): Flux<DocumentDto>
}