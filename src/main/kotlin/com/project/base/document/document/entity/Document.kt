package com.project.base.document.document.entity

import org.bson.types.ObjectId
import org.springframework.data.annotation.CreatedDate
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.LastModifiedDate
import java.time.Instant

data class Document(
    @Id var id: ObjectId?,
    var userId: String,
    var category: Category,
    var fileId: ObjectId,
    var processedDocumentId: ObjectId,
    @CreatedDate var createdDate: Instant,
    @LastModifiedDate var lastModifiedDate: Instant
) {
    enum class Category {
        RECEIPT
    }
}