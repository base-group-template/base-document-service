package com.project.base.document.document.domain.requests

data class DeleteDocument(val userId: String, val documentId: String)