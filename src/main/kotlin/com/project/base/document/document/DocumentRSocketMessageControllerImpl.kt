package com.project.base.document.document

import com.project.base.document.document.domain.DocumentDto
import com.project.base.document.document.domain.requests.*
import com.project.base.document.document.service.DocumentService
import org.springframework.messaging.handler.annotation.Headers
import org.springframework.messaging.handler.annotation.MessageMapping
import org.springframework.security.access.prepost.PreAuthorize
import org.springframework.stereotype.Controller
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono


@Controller
@PreAuthorize("isAuthenticated()")
class DocumentRSocketMessageControllerImpl(private val documentService: DocumentService) :
    DocumentRSocketMessageController {

    //TODO: the creation of roles for users is pending. once ready the roles can be validated
    //@PreAuthorize("hasAnyRole('ROLE_CUSTOMER','ROLE_OFFICEADMIN','ROLE_EMPLOYEE')")
    @MessageMapping("document.saveDocument")
    override fun saveDocument(request: SaveDocument): Mono<DocumentDto> =
        documentService.saveDocument(request.userId, request.data)

    @MessageMapping("document.findDocument")
    override fun findDocument(request: FindDocument): Mono<DocumentDto> =
        documentService.findDocument(request.userId, request.documentId)

    @MessageMapping("document.findAllDocuments")
    override fun findAllDocuments(@Headers metadata: Map<String, Any>, request: FindAllDocuments): Flux<DocumentDto> =
        documentService.findAllDocuments(request.userId)

    @MessageMapping("document.updateDocument")
    override fun updateDocument(request: UpdateDocument): Mono<DocumentDto> =
        documentService.updateDocument(request.userId, request.documentId, request.data)

    @MessageMapping("document.deleteDocument")
    override fun deleteDocument(request: DeleteDocument): Mono<Boolean> =
        documentService.deleteDocument(request.userId, request.documentId)

    @MessageMapping("document.deleteAllDocuments")
    override fun deleteAllDocuments(request: DeleteAllDocuments): Mono<Boolean> =
        documentService.deleteAllDocuments(request.userId)

    @MessageMapping("document.processFiles")
    override fun processFiles(@Headers metadata: Map<String, Any>, request: ProcessFiles): Flux<DocumentDto> =
        documentService.processFiles(metadata["token"] as String, request.documents)
}