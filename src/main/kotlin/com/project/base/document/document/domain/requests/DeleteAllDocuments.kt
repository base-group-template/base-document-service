package com.project.base.document.document.domain.requests

data class DeleteAllDocuments(val userId: String)