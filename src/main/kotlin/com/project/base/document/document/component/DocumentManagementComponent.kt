package com.project.base.document.document.component

import com.project.base.document.document.domain.CreateDocumentDto
import com.project.base.document.document.domain.DocumentDto
import com.project.base.document.document.domain.FileDataDto
import com.project.base.document.document.entity.Document
import org.bson.types.ObjectId
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface DocumentManagementComponent {
    fun saveDocument(createDocument: CreateDocumentDto, fileId: ObjectId, processedDocumentId: ObjectId): Mono<Document>

    fun findDocument(documentId: String): Mono<DocumentDto>

    fun findAllDocuments(userId: String): Flux<DocumentDto>

    fun deleteDocument(documentId: String): Mono<Boolean>

    fun saveFile(fileData: FileDataDto): Mono<ObjectId>

    fun deleteFile(fileId: ObjectId): Mono<Boolean>

    fun processFiles(token: String, createDocuments: List<CreateDocumentDto>): Flux<DocumentDto>

}