package com.project.base.document.document.entity

import org.bson.types.ObjectId
import org.springframework.data.annotation.Id
import org.springframework.data.mongodb.core.mapping.Field

data class ProcessedDocument(
    @Id val id: ObjectId,
    @Field("extracted_text") val extractedText: String,
    val data: Any
)