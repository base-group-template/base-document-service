package com.project.base.document.document.exception

class DocumentNotFoundException(message: String?) : Exception(message)