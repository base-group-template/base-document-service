package com.project.base.document.document.service

import com.project.base.document.document.component.DocumentManagementComponent
import com.project.base.document.document.domain.CreateDocumentDto
import com.project.base.document.document.domain.DocumentDto
import com.project.base.document.document.domain.FileDataDto
import org.bson.types.ObjectId
import org.springframework.stereotype.Service
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Service
class DocumentServiceImpl(
    private val documentManagementComponent: DocumentManagementComponent
) : DocumentService {
    override fun saveDocument(userId: String, data: FileDataDto): Mono<DocumentDto> =
        TODO("Not yet implemented")

    override fun findDocument(userId: String, documentId: String): Mono<DocumentDto> =
        documentManagementComponent.findDocument(documentId)

    override fun findAllDocuments(userId: String): Flux<DocumentDto> =
        documentManagementComponent.findAllDocuments(userId)

    override fun updateDocument(userId: String, documentId: String, data: FileDataDto): Mono<DocumentDto> =
        TODO("Not yet implemented")

    override fun deleteDocument(userId: String, documentId: String): Mono<Boolean> =
        documentManagementComponent.deleteDocument(documentId)

    override fun deleteAllDocuments(userId: String): Mono<Boolean> =
        TODO("Not yet implemented")

    override fun processFiles(token: String, createDocuments: List<CreateDocumentDto>): Flux<DocumentDto> =
        documentManagementComponent.processFiles(token, createDocuments)

    override fun deleteFile(fileId: ObjectId): Mono<Boolean> = documentManagementComponent.deleteFile(fileId)

}