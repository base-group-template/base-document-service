package com.project.base.document.document.component

import com.project.base.document.document.domain.*
import com.project.base.document.document.entity.Document
import com.project.base.document.document.entity.EntityManager
import com.project.base.document.processor.service.DocumentProcessorService
import org.bson.types.ObjectId
import org.springframework.stereotype.Component
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.kotlin.core.publisher.toFlux


@Component
class DocumentManagementComponentImpl(
    private val entityManager: EntityManager,
    private val documentProcessorService: DocumentProcessorService
) : DocumentManagementComponent {

    override fun saveDocument(
        createDocument: CreateDocumentDto,
        fileId: ObjectId,
        processedDocumentId: ObjectId
    ): Mono<Document> =
        entityManager.saveDocument(createDocument.toEntity(fileId, processedDocumentId))

    override fun findDocument(documentId: String): Mono<DocumentDto> =
        entityManager.findDocument(documentId).map { it.toDomain() }

    override fun findAllDocuments(userId: String): Flux<DocumentDto> =
        entityManager.findAllDocumentsByUserId(userId).map { it.toDomain() }

    override fun deleteDocument(documentId: String): Mono<Boolean> =
        entityManager.findDocument(documentId)
            .flatMap { document ->
                Mono.zip(
                    entityManager.deleteFile(document.fileId),
                    entityManager.deleteProcessedDocument(document.processedDocumentId),
                    entityManager.deleteDocument(documentId)
                ).map { it.t1 && it.t2 && it.t3 }
            }

    override fun saveFile(fileData: FileDataDto): Mono<ObjectId> = entityManager.saveFile(fileData.toEntity())

    override fun deleteFile(fileId: ObjectId): Mono<Boolean> = entityManager.deleteFile(fileId)

    override fun processFiles(token: String, createDocuments: List<CreateDocumentDto>): Flux<DocumentDto> =
        createDocuments.toFlux()
            .flatMap { createDocument ->
                saveFile(createDocument.file)
                    .flatMap { fileId ->
                        documentProcessorService.processDocument(token, fileId)
                            .flatMap { processedDocument ->
                                saveDocument(createDocument, fileId, processedDocument.id)
                                    .map { it.toDomain() }
                            }
                            .onErrorResume {
                                deleteFile(fileId).map { documentFallback() }
                            }
                    }
            }

}