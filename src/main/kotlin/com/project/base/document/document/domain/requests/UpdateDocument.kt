package com.project.base.document.document.domain.requests

import com.project.base.document.document.domain.FileDataDto

data class UpdateDocument(val userId: String, val documentId: String, val data: FileDataDto)