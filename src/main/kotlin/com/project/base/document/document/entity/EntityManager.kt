package com.project.base.document.document.entity

import org.bson.types.ObjectId
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface EntityManager {

    fun saveDocument(document: Document): Mono<Document>

    fun findDocument(id: String): Mono<Document>

    fun findAllDocumentsByUserId(userId: String): Flux<Document>

    fun deleteDocument(id: String): Mono<Boolean>

    fun deleteAllDocuemnts(userId: String): Mono<Boolean>

    fun saveFile(fileData: FileData): Mono<ObjectId>

    fun saveAllFiles(files: List<FileData>): Flux<ObjectId>

    fun findFile(id: ObjectId): Mono<FileData>

    fun deleteFile(id: ObjectId): Mono<Boolean>

    fun deleteAllFiles(ids: List<ObjectId>): Mono<Boolean>

    fun deleteProcessedDocument(id: ObjectId): Mono<Boolean>
}