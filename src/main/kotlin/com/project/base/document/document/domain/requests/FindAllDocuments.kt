package com.project.base.document.document.domain.requests

data class FindAllDocuments(val userId: String)