package com.project.base.document.document.entity

import java.io.InputStream

data class FileData(
    var name: String,
    var inputStream: InputStream,
    var mediaType: String,
    var extension: Extension

) {
    enum class Extension(val extension: String) {
        PDF("pdf"),
        JPG("jpg"),
        JPEG("jpeg"),
        FALLBACK("*")
    }
}