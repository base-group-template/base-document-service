package com.project.base.document.document.domain

import com.mongodb.BasicDBObject
import com.mongodb.DBObject
import com.mongodb.client.gridfs.model.GridFSFile
import com.project.base.document.document.entity.Document
import com.project.base.document.document.entity.FileData
import org.bson.types.ObjectId
import org.springframework.core.io.buffer.DataBuffer
import org.springframework.core.io.buffer.DefaultDataBufferFactory
import org.springframework.data.mongodb.gridfs.GridFsResource
import reactor.core.publisher.Flux
import java.io.ByteArrayInputStream
import java.io.InputStream
import java.nio.ByteBuffer
import java.time.Instant

const val MEDIA_TYPE_METADATA: String = "mediaType"
const val EXTENSION_METADATA: String = "extension"

fun Document.toDomain() =
    DocumentDto(
        id.toString(),
        userId,
        category.toDomain(),
        fileId.toString(),
        processedDocumentId.toString()
    )

private fun Document.Category.toDomain() =
    when (this) {
        Document.Category.RECEIPT -> DocumentDto.DocumentClass.RECEIPT
    }

private fun FileData.toDomain() =
    FileDataDto(
        name,
        inputStream.readAllBytes(),
        extension.toDomain(),
        mediaType
    )

private fun FileData.Extension.toDomain() =
    when (this) {
        FileData.Extension.JPEG -> FileDataDto.Extension.JPEG
        FileData.Extension.PDF -> FileDataDto.Extension.PDF
        FileData.Extension.JPG -> FileDataDto.Extension.JPG
        FileData.Extension.FALLBACK -> FileDataDto.Extension.FALLBACK
    }


fun CreateDocumentDto.toEntity(fileId: ObjectId, processedDocumentId: ObjectId): Document {
    val now = Instant.now()
    return Document(
        null,
        userId,
        Document.Category.RECEIPT,
        fileId,
        processedDocumentId,
        now,
        now
    )
}

fun FileDataDto.toEntity(): FileData =
    FileData(
        name,
        ByteArrayInputStream(bytes),
        mediaType,
        extension.toEntity()
    )

private fun FileDataDto.Extension.toEntity(): FileData.Extension =
    when (this) {
        FileDataDto.Extension.JPEG -> FileData.Extension.JPEG
        FileDataDto.Extension.PDF -> FileData.Extension.PDF
        FileDataDto.Extension.JPG -> FileData.Extension.JPG
        FileDataDto.Extension.FALLBACK -> FileData.Extension.FALLBACK
    }

fun GridFSFile.toFileData(): FileData =
    FileData(
        filename,
        GridFsResource.absent(filename).content,
        getMetadataProperty(MEDIA_TYPE_METADATA, metadata) as String,
        getMetadataProperty(EXTENSION_METADATA, metadata) as FileData.Extension
    )

fun FileData.toMetadata(): DBObject {
    val metaData: DBObject = BasicDBObject()
    metaData.put("mediaType", mediaType)
    metaData.put("extension", extension)

    return metaData
}

fun InputStream.toDataBufferFlux(): Flux<DataBuffer> {
    val bufferByte: ByteBuffer = ByteBuffer.wrap(readAllBytes())
    val defaultDataBufferFactory = DefaultDataBufferFactory()
    return Flux.just(defaultDataBufferFactory.wrap(bufferByte))
}

fun documentFallback(): DocumentDto =
    DocumentDto("","", DocumentDto.DocumentClass.RECEIPT, "", "")

private fun getMetadataProperty(propertyName: String, metadata: org.bson.Document?): Any? =
    if (metadata != null && metadata.containsKey(propertyName)) {
        metadata[propertyName]
    } else {
        Any()
    }