package com.project.base.document.document.domain

data class CreateDocumentDto(
    var userId: String,
    var category: Category,
    var file: FileDataDto
) {
    enum class Category {
        RECEIPT
    }
}