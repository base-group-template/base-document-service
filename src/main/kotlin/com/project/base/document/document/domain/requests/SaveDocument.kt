package com.project.base.document.document.domain.requests

import com.project.base.document.document.domain.FileDataDto

data class SaveDocument(val userId: String, val data: FileDataDto)