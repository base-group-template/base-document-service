package com.project.base.document.document.repository

import com.project.base.document.document.entity.Document
import org.bson.types.ObjectId
import org.springframework.data.mongodb.repository.ReactiveMongoRepository
import org.springframework.data.repository.query.Param
import reactor.core.publisher.Flux

interface DocumentRepository : ReactiveMongoRepository<Document, ObjectId> {
    fun findAllByUserId(@Param("userId") userId: String): Flux<Document>
}