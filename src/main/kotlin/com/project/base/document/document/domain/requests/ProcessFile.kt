package com.project.base.document.document.domain.requests

import com.project.base.document.document.domain.CreateDocumentDto

data class ProcessFile(
    val createDocument: CreateDocumentDto
)