package com.project.base.document.document.domain.requests

data class FindDocument(val userId: String, val documentId: String)