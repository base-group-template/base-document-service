package com.project.base.document.document.service

import com.project.base.document.document.domain.CreateDocumentDto
import com.project.base.document.document.domain.DocumentDto
import com.project.base.document.document.domain.FileDataDto
import org.bson.types.ObjectId
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

interface DocumentService {

    fun saveDocument(userId: String, data: FileDataDto): Mono<DocumentDto>

    fun findDocument(userId: String, documentId: String): Mono<DocumentDto>

    fun findAllDocuments(userId: String): Flux<DocumentDto>

    fun updateDocument(userId: String, documentId: String, data: FileDataDto): Mono<DocumentDto>

    fun deleteDocument(userId: String, documentId: String): Mono<Boolean>

    fun deleteAllDocuments(userId: String): Mono<Boolean>

    fun processFiles(token: String, createDocuments: List<CreateDocumentDto>): Flux<DocumentDto>

    fun deleteFile(fileId: ObjectId): Mono<Boolean>
}