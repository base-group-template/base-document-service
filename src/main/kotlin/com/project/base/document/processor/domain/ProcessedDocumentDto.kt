package com.project.base.document.processor.domain

import org.bson.types.ObjectId

data class ProcessedDocumentDto(val id: ObjectId)