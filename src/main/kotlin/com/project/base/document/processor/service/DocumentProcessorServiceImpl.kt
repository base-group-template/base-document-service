package com.project.base.document.processor.service

import com.project.base.document.processor.configuration.DocumentProcessorServiceConfiguration
import com.project.base.document.processor.domain.ProcessedDocumentDto
import org.bson.types.ObjectId
import org.slf4j.LoggerFactory
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.stereotype.Service
import org.springframework.web.reactive.function.BodyInserters
import org.springframework.web.reactive.function.client.WebClient
import reactor.core.publisher.Mono

@Service
class DocumentProcessorServiceImpl(pythonServiceConfiguration: DocumentProcessorServiceConfiguration) :
    DocumentProcessorService {
    private val webClient: WebClient = pythonServiceConfiguration.webClient()

    companion object {
        private val logger = LoggerFactory.getLogger(DocumentProcessorServiceImpl::class.java)
    }

    override fun processDocument(token: String, fileId: ObjectId): Mono<ProcessedDocumentDto> =
        webClient
            .post()
            .uri("/document-processor/process-document/")
            .headers { httpHeaders -> setAuthenticationHeaders(httpHeaders, token) }
            .accept(MediaType.APPLICATION_JSON)
            .body(BodyInserters.fromFormData("id", fileId.toString()))
            .exchangeToMono { clientResponse -> clientResponse.bodyToMono(ProcessedDocumentDto::class.java) }

    private fun setAuthenticationHeaders(httpHeaders: HttpHeaders, token: String): HttpHeaders {
        httpHeaders[HttpHeaders.AUTHORIZATION] = "Bearer $token"
        httpHeaders[HttpHeaders.CONTENT_TYPE] = MediaType.APPLICATION_JSON_VALUE
        return httpHeaders
    }
}