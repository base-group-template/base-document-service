package com.project.base.document.processor.service

import com.project.base.document.processor.domain.ProcessedDocumentDto
import org.bson.types.ObjectId
import reactor.core.publisher.Mono

interface DocumentProcessorService {
    fun processDocument(token: String, fileId: ObjectId): Mono<ProcessedDocumentDto>
}