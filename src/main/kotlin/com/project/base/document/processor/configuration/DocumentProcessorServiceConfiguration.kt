package com.project.base.document.processor.configuration

import com.project.base.document.core.configuration.web.WebClientLogger
import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Configuration
import org.springframework.http.client.reactive.ReactorClientHttpConnector
import org.springframework.web.reactive.function.client.WebClient
import reactor.netty.http.client.HttpClient

@Configuration
class DocumentProcessorServiceConfiguration {

    @Value("\${services.python.host}")
    private val host: String = ""

    @Value("\${services.python.port}")
    private val port: Int = 0

    fun webClient(): WebClient =
        WebClient.builder()
            .baseUrl("$host:$port")
            .clientConnector(ReactorClientHttpConnector(httpClient()))
            .build()

    private fun httpClient(): HttpClient {
        return HttpClient
            .create()
            .doOnRequest { _, connection ->
                connection.addHandlerFirst(WebClientLogger(HttpClient::class.java))
            }
    }

}