package com.project.base.document.core.configuration.security

import org.springframework.security.core.Authentication
import org.springframework.security.core.context.ReactiveSecurityContextHolder
import org.springframework.security.oauth2.jwt.Jwt
import org.springframework.stereotype.Component
import reactor.core.publisher.Mono

@Component
class AuthenticationService {

    fun getAuthentication(): Mono<Authentication> =
        ReactiveSecurityContextHolder
            .getContext()
            .map { it.authentication }

    fun getAuthenticatedUserToken(): Mono<String> = getPrincipal().map { it.tokenValue }

    fun getUserId(): Mono<String> = getPrincipal().map { it.claims["sub"] as String }

    private fun getPrincipal(): Mono<Jwt> = getAuthentication().map { it.principal as Jwt }
}