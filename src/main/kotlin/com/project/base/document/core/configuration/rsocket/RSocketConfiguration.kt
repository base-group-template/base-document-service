package com.project.base.document.core.configuration.rsocket

import io.rsocket.metadata.WellKnownMimeType
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.core.codec.StringDecoder
import org.springframework.http.codec.cbor.Jackson2CborDecoder
import org.springframework.http.codec.cbor.Jackson2CborEncoder
import org.springframework.http.codec.json.Jackson2JsonDecoder
import org.springframework.http.codec.json.Jackson2JsonEncoder
import org.springframework.messaging.rsocket.MetadataExtractorRegistry
import org.springframework.messaging.rsocket.RSocketStrategies
import org.springframework.messaging.rsocket.annotation.support.RSocketMessageHandler
import org.springframework.util.MimeTypeUtils

@Configuration
class RSocketConfiguration {

    companion object {
        private val MESSAGE_RSOCKET_AUTHENTICATION =
            MimeTypeUtils.parseMimeType(WellKnownMimeType.MESSAGE_RSOCKET_AUTHENTICATION.string)
        private val APPLICATION_JSON = MimeTypeUtils.parseMimeType(WellKnownMimeType.APPLICATION_JSON.string)
        private val TEXT_PLAIN = MimeTypeUtils.parseMimeType(WellKnownMimeType.TEXT_PLAIN.string)
    }

    @Bean
    fun rsocketMessageHandler(): RSocketMessageHandler {
        val handler = RSocketMessageHandler()
        handler.rSocketStrategies = rsocketStrategies()
        return handler
    }

    @Bean
    fun rsocketStrategies(): RSocketStrategies =
        RSocketStrategies.builder()
            .metadataExtractorRegistry { registry: MetadataExtractorRegistry ->
                registry.metadataToExtract(TEXT_PLAIN, String::class.java, "token")
            }
            .encoders { encoders ->
                encoders.addAll(
                    listOf(
                        Jackson2CborEncoder(),
                        Jackson2JsonEncoder()
                    )
                )
            }
            .decoders { decoders ->
                decoders.addAll(
                    listOf(
                        Jackson2CborDecoder(),
                        Jackson2JsonDecoder(),
                        StringDecoder.allMimeTypes()
                    )
                )
            }
            .build()
}
