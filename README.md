# Read Me First

Base project for a service programmed in kotlin.

The following was discovered as part of building this project:

* The original package name 'com.base-kotlin-service.base-kotlin-service' is invalid and this project uses '
  com.basekotlinservice.basekotlinservice' instead.

# Getting Started

### Reference Documentation

For further reference, please consider the following sections:

* [Official Apache Maven documentation](https://maven.apache.org/guides/index.html)
* [Spring Boot Maven Plugin Reference Guide](https://docs.spring.io/spring-boot/docs/2.3.4.RELEASE/maven-plugin/reference/html/)
* [Create an OCI image](https://docs.spring.io/spring-boot/docs/2.3.4.RELEASE/maven-plugin/reference/html/#build-image)
* [Coroutines section of the Spring Framework Documentation](https://docs.spring.io/spring/docs/5.2.9.RELEASE/spring-framework-reference/languages.html#coroutines)
* [OAuth2 Resource Server](https://docs.spring.io/spring-boot/docs/2.3.4.RELEASE/reference/htmlsingle/#boot-features-security-oauth2-server)
* [Spring Data Reactive MongoDB](https://docs.spring.io/spring-boot/docs/2.3.4.RELEASE/reference/htmlsingle/#boot-features-mongodb)

